import json
import sys


def collect(method, files):
    for file in files:
        with open(file, 'r') as f:
            history = json.load(f)
            for event in history['events']:
                is_return = event['kind'] == 'return'
                is_void = event['invocation']['method']['void']
                name = event['invocation']['method']['name']
                if is_return and name == method:
                    print(event['value'])


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: {} <method> <history-files>'.format(sys.argv[0]))
        sys.exit()
    collect(sys.argv[1], sys.argv[2:])