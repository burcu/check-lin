import json
import sys


def encode_argument(arg):
    if isinstance(arg, dict):
        return '{' + ', '.join('{} {}'.format(k, arg[k]) for k in arg) + '}'
    elif isinstance(arg, list):
        return '[' + ' '.join(str(v) for v in arg) + ']'
    elif isinstance(arg, set):
        return '#{' + ' '.join(str(el) for el in arg) + '}'
    elif arg == 'null':
        return 'nil'
    elif isinstance(arg, bool):
        return 'true' if arg else 'false'
    elif isinstance(arg, str) and arg in ['true', 'false']:
        return arg
    # elif isinstance(arg, str):
    #     return '\"' + arg + '\"'
    else:
        return str(arg)


def transform_event(event):
    is_void = event['invocation']['method']['void']
    is_call = event['kind'] == 'call'

    if is_void:
        ret_val = ''
    elif is_call:
        ret_val = 'nil'
    else:
        ret_val = encode_argument(event['value'])

    args = event['invocation']['arguments']
    args_str = ' '.join(encode_argument(arg) for arg in args)
    if is_void and len(args) == 0:
        value = 'nil'
    elif is_void and len(args) == 1:
        value = args_str
    elif is_void:
        value = '[' + args_str + ']'
    elif not is_void and len(args) == 0:
        if ret_val[0] == '{' or ret_val[0] == '[':
            ret_val = '\"' + ret_val + '\"'
        value = ret_val
    else:
        value = '[' + ' '.join([args_str, ret_val]) + ']'

    return '{{:process {}, :type {}, :f :{}, :value {}}}'.format(
        event['sid'],
        ':invoke' if is_call else ':ok',
        event['invocation']['method']['name'],
        value)


def transform(file):
    with open(file, 'r') as f:
        history = json.load(f)
        return '[' + '\n'.join(transform_event(event) for event in history['events']) + ']'


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: {} <history-file>'.format(sys.argv[0]))
        sys.exit()
    print(transform(sys.argv[1]))