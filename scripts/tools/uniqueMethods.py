import json
import sys


def collect(files):
    methods = set()
    for file in files:
        with open(file, 'r') as f:
            history = json.load(f)
            for event in history['events']:
                methods.add(event['invocation']['method']['name'])
    print(methods)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: {} <history-files>'.format(sys.argv[0]))
        sys.exit()
    collect(sys.argv[1:])